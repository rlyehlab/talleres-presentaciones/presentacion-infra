## Rlyeh Hacklab infraestructure presentation and explanation

this presentatios was created with reveal.js

This presentation includes:
* About Rlyeh
* History of infra
* Elements used
* Things missing (at date)



## License

![CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/88x31.png)  
All work is under *[Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es)*
